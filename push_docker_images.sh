#!/bin/bash

docker pull registry.gitlab.com/wonderpleio/videocall:signaling-1.2.6
docker pull registry.gitlab.com/wonderpleio/videocall:web-1.2.6
docker tag registry.gitlab.com/wonderpleio/videocall:signaling-1.2.6 registry.gitlab.com/pleio/videocall-infra:signaling-1.2.6
docker tag registry.gitlab.com/wonderpleio/videocall:web-1.2.6 registry.gitlab.com/pleio/videocall-infra:web-1.2.6
docker push registry.gitlab.com/pleio/videocall-infra:signaling-1.2.6
docker push registry.gitlab.com/pleio/videocall-infra:web-1.2.6
