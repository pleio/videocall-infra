# videocall-infra

Automated way of deploying the videocall to the Pleio infrastructure

# Helm

## Installing the secrets

```
helm install -n videocall -f videocall-secrets/values.odcn.test.yaml videocall-test-secrets videocall-secrets/
```

## Installing the application

```
helm install -n videocall -f videocall/values.odcn.test.yaml videocall-test videocall/
```


## Creating a Room

```
curl -X 'POST' 'https://videobellen.pleio-test.nl/api/v1/room/'  -H 'accept: application/json'  -H 'Content-Type: application/json'  -d '{  "date": "2022-10-20",  "time_start": "9:00",  "time_finish": "17:00",  "language": "en",  "users": {    "name_host": "Belastingdienst",    "name_guest": "John Doe"  }}'
```

