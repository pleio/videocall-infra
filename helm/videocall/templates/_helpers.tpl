{{- define "videocall.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "videocall.secretsName" -}}
{{- printf "%s-secrets" ((include "videocall.name" .)) -}}
{{- end -}}

{{- define "videocall.tlsSecretName" -}}
{{- printf "tls-%s" ((include "videocall.name" .)) -}}
{{- end -}}

{{- define "videocall.initContainerName" -}}
{{- printf "%s-init" ((include "videocall.name" . )) -}}
{{- end -}}

{{- define "videocall.nginxName" -}}
{{- printf "%s-nginx" ((include "videocall.name" . )) -}}
{{- end -}}

{{- define "videocall.webserverName" -}}
{{- printf "%s-webserver" ((include "videocall.name" . )) -}}
{{- end -}}

{{- define "videocall.signalingName" -}}
{{- printf "%s-signaling" ((include "videocall.name" . )) -}}
{{- end -}}

{{- define "videocall.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "videocall.labels" -}}
helm.sh/chart: {{ include "videocall.chart" . }}
{{ include "videocall.selectorLabels" . }}
{{- end -}}

{{- define "videocall.selectorLabels" -}}
app.kubernetes.io/name: {{ include "videocall.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Component }}
app.kubernetes.io/component: {{ .Component }}
{{- end }}
{{- end -}}

{{- define "videocall.hosts" -}}
- {{ . | quote }}
{{- if not (hasPrefix "*" .) }}
- {{ printf "www.%s" . | quote }}
{{- end }}
{{- end -}}
