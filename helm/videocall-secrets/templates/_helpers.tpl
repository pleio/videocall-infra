{{- define "videocall.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "videocall.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "videocall.labels" -}}
helm.sh/chart: {{ include "videocall.chart" . }}
{{ include "videocall.selectorLabels" . }}
{{- end -}}

{{- define "videocall.selectorLabels" -}}
app.kubernetes.io/name: {{ include "videocall.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Component }}
app.kubernetes.io/component: {{ .Component }}
{{- end }}
{{- end -}}
